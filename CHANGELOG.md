v0.1.0

## CHANGELOG

### v0.1.0

>**This release marks the addition of the most basic features I needed for this script**

[**971864e**](https://github.com/adonaac/versus/commit/971864ed671eddf57a8d9382cde5a7eb2159a7d3) (**2015-02-24 12:03:37**)

#### Additions

* Added todo and done tags - [9c91f1c](https://github.com/adonaac/versus/commit/9c91f1cca0f18fc1dd9fe1d2bb59c9f4196f9e3b) (2015-02-24 11:57:42)

#### Removed

* Removed old changelog examples - [0e7cd10](https://github.com/adonaac/versus/commit/0e7cd1063e219504aa3b77619f49e8d723770940) (2015-02-24 03:38:22)

---

### v0.0.7

#### Additions

* Added todo and done tags - [9c91f1c](https://github.com/adonaac/versus/commit/9c91f1cca0f18fc1dd9fe1d2bb59c9f4196f9e3b) (2015-02-24 11:57:42)

#### Removed

* Removed old changelog examples - [0e7cd10](https://github.com/adonaac/versus/commit/0e7cd1063e219504aa3b77619f49e8d723770940) (2015-02-24 03:38:22)

---

### v0.0.6

>**Testing release_patch**

[**819bbb4**](https://github.com/adonaac/versus/commit/819bbb48fc5c90490efab311e60da0284736dfd2) (**2015-02-24 03:29:16**)

#### Additions

* Add -t option to target either github or bitbucket, omitting it defaults to github - [cb5def2](https://github.com/adonaac/versus/commit/cb5def277fcb123798e4abc0ceae1d068c811d98) (2015-02-22 13:11:09)
* Add -p option to set the changelog file's output path - [cbebc64](https://github.com/adonaac/versus/commit/cbebc64718fa0efadc1852ad1783d93e042e3f97) (2015-02-22 13:14:42)
* Added -n option that sets the name of the output file, omitting it defaults to CHANGELOG - [ca205cc](https://github.com/adonaac/versus/commit/ca205cc18f939993ff675031f5443fd97b431652) (2015-02-22 13:28:23)
* Added post-commit git hook - [072e45f](https://github.com/adonaac/versus/commit/072e45f5c43c6ee157cb0b8e717a1fc845431129) (2015-02-22 20:50:58)
* Added release_patch tag - [cb04e0c](https://github.com/adonaac/versus/commit/cb04e0cdd319ede41610b744c5e3b4581dbea981) (2015-02-24 03:28:47)

#### Fixes

* Fixed auto commit on changelog regeneration - [68ff603](https://github.com/adonaac/versus/commit/68ff603aed4d21e92fff886f01a764d818de504a) (2015-02-22 21:25:25)

#### Updates

* Update post-commit hook to also commit the regenerated changelog - [7e550bd](https://github.com/adonaac/versus/commit/7e550bdc30706f28649f2068372acebf9e8cc1de) (2015-02-22 21:24:22)

---

### v0.0.5


#### Fixes

* Fixed auto commit on changelog regeneration - [68ff603](https://github.com/adonaac/versus/commit/68ff603aed4d21e92fff886f01a764d818de504a) (2015-02-22 21:25:25)

#### Updates

* Update post-commit hook to also commit the regenerated changelog - [7e550bd](https://github.com/adonaac/versus/commit/7e550bdc30706f28649f2068372acebf9e8cc1de) (2015-02-22 21:24:22)

---

### v0.0.4

#### Additions

* Added post-commit git hook - [072e45f](https://github.com/adonaac/versus/commit/072e45f5c43c6ee157cb0b8e717a1fc845431129) (2015-02-22 20:50:58)

---

### v0.0.3

#### Additions

* Added -n option that sets the name of the output file, omitting it defaults to CHANGELOG - [ca205cc](https://github.com/adonaac/versus/commit/ca205cc18f939993ff675031f5443fd97b431652) (2015-02-22 13:28:23)

---

### v0.0.2

#### Additions

* Add -p option to set the changelog file's output path - [cbebc64](https://github.com/adonaac/versus/commit/cbebc64718fa0efadc1852ad1783d93e042e3f97) (2015-02-22 13:14:42)

---

### v0.0.1

#### Additions

* Add -t option to target either github or bitbucket, omitting it defaults to github - [cb5def2](https://github.com/adonaac/versus/commit/cb5def277fcb123798e4abc0ceae1d068c811d98) (2015-02-22 13:11:09)

---

