# versus

A git log based auto changelog generator and auto versioning system.

## Usage

Assuming you have Lua installed and can call it from the command line, simply drop the [module](https://github.com/adonaac/versus/blob/master/versus) somewhere and call:

```shell
$ lua versus username repository local_repository_path
```

If you want to be able to call `versus` directly:

```shell
$ which lua
```

Will return Lua's path on your system. Take this path and make it the first line of the `versus` file:

```shell
#!/c/MinGW/bin/lua
```

Then you give yourself permission to run the file:

```shell
$ chmod u+x versus
```

And you'll be able to call:

```shell
$ versus username repository local_repository_path
```

## Example

What this script does is simply take the output of `git log` from the target repository, parse it based on certain tags inside each commit, and then generate the output markdown styled changelog fie. The changelog for this repository is an example [CHANGELOG.md](https://github.com/adonaac/versus/blob/master/CHANGELOG.md), and the accompanying [commits](https://github.com/adonaac/versus/commits/master).

All you have to do to use it is:

```shell
$ versus username repository local_repository_path
```

So for instance, if I were to generate the changelog for this repository I'd do:

```shell
$ versus adonaac versus ~/dev/versus
```

And then the [CHANGELOG.md](https://github.com/adonaac/versus/blob/master/CHANGELOG.md) file will be generated in `~/dev/versus`.

## Options

A few options are available. Options must come only after `username`, `repository` and `local_repository_path`.

**`-t`** `target:` targets either github or bitbucket for linking commit messages, defaults to github if omitted

**`-p`** `path:` sets the changelog file's output path, defaults to `local_repository_path` if omitted

**`-n`** `name:` sets the changelog file's output name, defaults to `CHANGELOG` if omitted

```shell
$ versus adonaac secret_game ~/dev/secret_game -t bitbucket -p ~/dev/changelogs -n SECRET_GAME_CHANGELOG
```

## Version

The versioning system uses the default format of `MAJOR.MINOR.PATCH`. Specific tags added to a commit can increase each of the fields. Increasing `MINOR` resets `PATCH`, increasing `MAJOR` resets `MINOR` and `PATCH`.

## Tags

Each commit can have multiple changelog entries in the following format: `tag message`. Each entry is divided by `;`, so a single commit with multiple entries will be something like: `tag1 message1; tag2 message2; tag3 message3`. A `message` is the text that will be printed on the changelog. A `tag` is a special code that helps divide changelog entries as well as control the versioning system. Available tags are:

**`addi:`** adds the message to the changelog under the `Additions` section

**`addp:`** adds the message to the changelog under the `Additions` section and increases `PATCH` by 1

**`docn:`** adds the message to the `documentation needed` list

**`doca:`** matches the message to previous `documentation needed` messages, unmatched messages will generate a `WARNING` on the top of the changelog

**`done:`** removes a todo element from the todo list, the number passed (`done #23`, for instance) has to match an existing number in the todo list

**`fixi:`** adds the message to the changelog under the `Fixes` section

**`fixp:`** adds the message to the changelog under the `Fixes` section and increases `PATCH` by 1

**`remi:`** adds the message to the changelog under the `Removed` section

**`remp:`** adds the message to the changelog under the `Removed` section and increases `PATCH` by 1

**`reni:`** adds the message to the changelog under the `Renamed` section

**`renp:`** adds the message to the changelog under the `Renamed` section and increases `PATCH` by 1

**`updi:`** adds the message to the changelog under the `Updates` section

**`updp:`** adds the message to the changelog under the `Updates` section and increases `PATCH` by 1

**`release_patch:`** adds the message to the changelog as a minor release and increases `PATCH` by 1

**`release_minor:`** adds the message to the changelog as a minor release and increases `MINOR` by 1

**`release_major:`** adds the message to the changelog as a major release and increases `MAJOR` by 1

**`todo:`** adds the message to the changelog under the `TODO` section with a random number as its identifier (to be matched with `done`)

## post-commit

A useful [post-commit](https://github.com/adonaac/versus/blob/master/post-commit) hook was created to help with automatically regenerating changelogs after each commit.
Simply place that file inside `.git/hooks/` and change the `USER` and `REPO` variables accordingly.

## LICENSE

You can do whatever you want with this. See the [LICENSE](https://github.com/adonaac/versus/blob/master/LICENSE).
